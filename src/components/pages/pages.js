import React , {Component} from 'react';
import axios from 'axios';
import {Link, Redirect} from 'react-router-dom';
import './page.css';

class Pages extends Component {

    constructor(props){
        super(props)
        this.state = {
            session: localStorage.getItem('session'),
            data:     {
                                    "tests_details": [
                                                    {
                                                        "name": "W 1",
                                                        "questions": "3",
                                                        "attempted": 0,
                                                        "time_taken": "0:30:00",
                                                        "time_allowed": "00:40:00",
                                                        "id": "18",
                                                        "state": "notstarted",
                                                        "form_variable": "cmid",
                                                        "form_action": "/mod/quiz/startattempt.php",
                                                        "test_id": "24",
                                                        "form_id": "24",
                                                        "access": 1,
                                                        "button_class": "attempt",
                                                        "button_text": "Attempt",
                                                        "button_icon": "<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>",
                                                        "time_text": "Time Limit",
                                                        "time_info": "00:40:00",
                                                        "icon_class": "fas fa-hand-point-up",
                                                        "progressbar_class": "progress-bar progress-bar-success blue-sharp"
                                                    },
                                                    {
                                                        "name": "W 2",
                                                        "questions": "4",
                                                        "attempted": 0,
                                                        "time_taken": "0:30:00",
                                                        "time_allowed": "00:50:00",
                                                        "id": "19",
                                                        "state": "notstarted",
                                                        "form_variable": "cmid",
                                                        "form_action": "/mod/quiz/startattempt.php",
                                                        "test_id": "25",
                                                        "form_id": "25",
                                                        "access": 1,
                                                        "button_class": "attempt",
                                                        "button_text": "Attempt",
                                                        "button_icon": "<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>",
                                                        "time_text": "Time Limit",
                                                        "time_info": "00:50:00",
                                                        "icon_class": "fas fa-hand-point-up",
                                                        "progressbar_class": "progress-bar progress-bar-success blue-sharp"
                                                    },
                                                    {
                                                        "name": "W 3",
                                                        "questions": "3",
                                                        "attempted": 0,
                                                        "time_taken": "0:30:00",
                                                        "time_allowed": "00:40:00",
                                                        "id": "20",
                                                        "state": "notstarted",
                                                        "form_variable": "cmid",
                                                        "form_action": "/mod/quiz/startattempt.php",
                                                        "test_id": "26",
                                                        "form_id": "26",
                                                        "access": 1,
                                                        "button_class": "attempt",
                                                        "button_text": "Attempt",
                                                        "button_icon": "<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>",
                                                        "time_text": "Time Limit",
                                                        "time_info": "00:40:00",
                                                        "icon_class": "fas fa-hand-point-up",
                                                        "progressbar_class": "progress-bar progress-bar-success blue-sharp"
                                                    },
                                                    {
                                                        "name": "W 4",
                                                        "questions": "4",
                                                        "attempted": 0,
                                                        "time_taken": "0:30:00",
                                                        "time_allowed": "00:50:00",
                                                        "id": "21",
                                                        "state": "notstarted",
                                                        "form_variable": "cmid",
                                                        "form_action": "/mod/quiz/startattempt.php",
                                                        "test_id": "27",
                                                        "form_id": "27",
                                                        "access": 1,
                                                        "button_class": "attempt",
                                                        "button_text": "Attempt",
                                                        "button_icon": "<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>",
                                                        "time_text": "Time Limit",
                                                        "time_info": "00:50:00",
                                                        "icon_class": "fas fa-hand-point-up",
                                                        "progressbar_class": "progress-bar progress-bar-success blue-sharp"
                                                    }
                                                ]
                                }
        };
    }

    componentWillMount() {
        this.fetchData(this.props.match.params.page);
    }

    componentDidUpdate() {
        this.fetchData(this.props.match.params.page);
    }

    fetchData(page) {
        axios.get(`http://pte.ptepracticetests.com/dashboard/index.php?action=showpage&page=${page}&result=json`)
        .then(response => {
            console.log(response);
            this.setState({data: response})
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    getCards(){
        if (this.state.session !== "true") {
            return <Redirect to='/login'/>;
        }

        return this.state.data.tests_details.map(function(object, i){
            return <div className="col-md-3 card" key={i}>
                        <h6>
                            <b>Name</b>
                        </h6> 
                        <p>{object.name}</p>
                        <h6><b>Question Attempted</b></h6>
                        <p>{object.attempted}</p>
                        <h6><b>State</b></h6>
                        <p>{object.state == "notstarted" ? "Not Started" : "Started"}</p>
                        <h6><b>Time</b></h6>
                        <p>{object.time_info}</p>
                        <button className={object.button_class}>{object.button_text}</button>
                    </div>;
        });
      }

    render()
    {
        return (
            <div className="containers">
                <div className="row">
                    <div className="col-md-2">
                        <Link to={'reading'} className="nav-link">Reading</Link><br/>
                        <Link to={'writing'} className="nav-link">Writing</Link><br/>
                        <Link to={'listening'} className="nav-link">Listening</Link><br/>
                    </div>
                    <div className="col-md-10">
                        <div className="row">
                            {this.getCards()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Pages;
