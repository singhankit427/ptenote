import React , {Component} from 'react';
import '../../style.css';
import './login.css';
import axios from 'axios';
import logo from './ptepreacticetests_logo_web.png';
import { Redirect } from 'react-router-dom'

class Login extends Component {

    constructor(props) {
        super(props);
        this.onChangeUserName = this.onChangeUserName.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
        this.state = {
          username: '',
          password: '',
          session: localStorage.getItem('session'),
        }

    }

    onSubmit(){
        axios.post('https://pte.ptepracticetests.com/dashboard/api/login', {
            username: 'ankitsingh_demo',
            password: 'Password@123'
           },
           {
             headers: {
               'Content-Type': 'application/json'
             }
           })
        .then(response => {
            if(response.status == "success"){
                localStorage.setItem('session', true)
                this.setState({session: true})
            }else{
                alert('Unable to login');
            }
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    onChangeUserName(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    render()
    {
        if (this.state.session === "true" ) {
            return <Redirect to='/pages/speaking'/>;
        }

        return (
            <div className="login-page-body">
                <div className="login-page">
                    <div className="form">        
                        <a href="https://pte.ptepracticetests.com/dashboard/pte.ptepracticetests.com">
                            <img src={logo} alt="" className="logo" />
                        </a>        
                        
                            <input type="text" placeholder="username" name="username" value={this.state.username}
                            onChange={this.onChangeUserName} />
                            <input type="password" placeholder="password" name="password" value={this.state.password}
                            onChange={this.onChangePassword} />
                            <button onClick={this.onSubmit}>login</button>
                        
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
